//Флаг режима работы WiFi, по умолчанию режим точки доступа
#define use_ap

#ifndef use_ap
    #define ssid_hotspot ""
    #define psk ""
#endif
#ifdef use_ap
    #define ssid_ap "DRL"
    #define ap_psk "example123"
#endif