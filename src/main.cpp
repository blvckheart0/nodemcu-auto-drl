#include <FastLed.h>
#include <sstream>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <config.cpp>

#pragma region Настройка WiFi
#ifdef use_ap
const char *ssid = ssid_ap;
const char *password = ap_psk;
bool ap_mode = true;
#endif
#ifndef use_ap
const char *ssid = ssid_hotspot;
const char *password = psk;
bool ap_mode = false;
#endif

#pragma endregion

#pragma region Конфигурация ленты
//Количество светодиодов на ленте левой фары
#define ledCount 39
//Пин левой фары
#define leftPin D6
#define leftInput 3
//Пин правой фары
#define rightPin 16
#define rightInput 13
//Скорость эффекта в мс
#define effectSpeed 14
//Цвет режима ДХО
#define drlColor CHSV(70, 70, 255)
//Цвет в режиме поворотника
#define effectColor CHSV(39, 255, 255)
//Цвет загрузки
#define bootColor CRGB::DarkRed
//Длина полоски поворотника
#define effectStripLength 20

CRGB leftStrip[ledCount];
CRGB rightStrip[ledCount];

bool leftEffectCompleted = true;
bool rightEffectCompleted = true;

int leftEffectCounter = 0;
int rightEffectCounter = 0;

std::string mode = "default";

#pragma endregion

void setDrl(CRGB *strip, int brightness)
{
  for (int index = 0; index < ledCount; index++)
  {
    CHSV currentColor = drlColor;
    currentColor.v = brightness;
    strip[index] = currentColor;
  }
}

void mainEffect(CRGB *currentStrip, std::string stripName)
{
  int *counter = nullptr;
  bool *completePointer = nullptr;
  if (stripName == "right")
  {
    counter = &rightEffectCounter;
    completePointer = &rightEffectCompleted;
  }
  else if (stripName == "left")
  {
    counter = &leftEffectCounter;
    completePointer = &leftEffectCompleted;
  }

  if (!*completePointer)
  {
    setDrl(currentStrip, 50);
    if (*counter < ledCount)
    {
      currentStrip[*counter] = effectColor;
    }
    for (int index = *counter; index > *counter - effectStripLength; index--)
    {
      if (index > -1 && index < ledCount)
      {
        currentStrip[index] = effectColor;
      }
    }
    *counter = *counter + 1;
    if (*counter > ledCount + effectStripLength - 1)
    {
      *completePointer = true;
    }
  }
}

void bootEffect(){
    for(int index = 0; index < ledCount; index++){
        leftStrip[index] = bootColor;
        rightStrip[index] = bootColor;
        FastLED.show();
        delay(10);
    }
    for(int index = 0; index < ledCount; index++){
        leftStrip[index] = drlColor;
        rightStrip[index] = drlColor;
        FastLED.show();
        delay(8);
    }
}

void setup()
{
  pinMode(leftInput, INPUT);
  pinMode(rightInput, INPUT);
  pinMode(leftPin,OUTPUT);
  pinMode(rightPin,OUTPUT);
  FastLED.addLeds<WS2812B, leftPin, GRB>(leftStrip, ledCount);
  FastLED.addLeds<WS2812B, rightPin, GRB>(rightStrip, ledCount);
  bootEffect();
  setDrl(leftStrip, 255);
  setDrl(rightStrip, 255);

  #pragma region OTA Настройка
    if (ap_mode == true)
    {
      WiFi.softAP(ssid, password);
    }
    else{
        WiFi.mode(WIFI_STA);
        WiFi.begin(ssid, password);
    }
    ArduinoOTA.setHostname("DRL");  
    ArduinoOTA.onStart([]()
    {
      String type;
      if (ArduinoOTA.getCommand() == U_FLASH)
      {
        type = "sketch";
      }
      else
      {
        type = "filesystem";
      }
    });
    
    ArduinoOTA.begin();

  #pragma endregion
  FastLED.clear();
  FastLED.setBrightness(100);
  FastLED.show();
}

void loop()
{

  if (mode == "default")
  {
    if (digitalRead(leftInput) == 0 && leftEffectCompleted == true)
    {
      leftEffectCounter = 0;
      leftEffectCompleted = false;
    }
    else
    {
      setDrl(leftStrip, 255);
    }
    if (digitalRead(rightInput) == 0 && rightEffectCompleted == true)
    {
      rightEffectCounter = 0;
      rightEffectCompleted = false;
    }
    else
    {
      setDrl(rightStrip, 255);
    }
    EVERY_N_MILLISECONDS(effectSpeed)
    {
      mainEffect(leftStrip, "left");
      mainEffect(rightStrip, "right");
      FastLED.show();
    }
  }
  else if (mode == "police")
  {
    fill_solid(leftStrip, ledCount, CRGB::Blue);
    fill_solid(rightStrip, ledCount, CRGB::Red);
    FastLED.show();
    delay(200);
    fill_solid(leftStrip, ledCount, CRGB::Red);
    fill_solid(rightStrip, ledCount, CRGB::Blue);
    FastLED.show();
    delay(200);
  }
  ArduinoOTA.handle();
  

}